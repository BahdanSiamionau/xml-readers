package com.epam.xmlparsers.helper;

import java.util.HashMap;
import javax.servlet.http.HttpServletRequest;

import com.epam.xmlparsers.command.DOMParserCommand;
import com.epam.xmlparsers.command.ICommand;
import com.epam.xmlparsers.command.IndexCommand;
import com.epam.xmlparsers.command.SAXParserCommand;
import com.epam.xmlparsers.command.StAXParserCommand;
import com.epam.xmlparsers.resources.ParserConstants;

public final class RequestHelper {

	private static RequestHelper instance = null;
	private HashMap <String, ICommand> commands = new HashMap <String, ICommand> ();
	
	private RequestHelper() {
		commands.put(ParserConstants.SAX, new SAXParserCommand());
		commands.put(ParserConstants.STAX, new StAXParserCommand());
		commands.put(ParserConstants.DOM, new DOMParserCommand());
		commands.put(ParserConstants.INDEX, new IndexCommand());
	}
	
	public ICommand getCommand(HttpServletRequest request) {
		String action = request.getParameter(ParserConstants.COMMAND);
		ICommand command = commands.get(action);
		if (command == null) {
			command = new IndexCommand();
		}
		return command;
	}

	public static RequestHelper getInstance() {
		if (instance == null) {
			instance = new RequestHelper();
		}
		return instance;
	}
}

