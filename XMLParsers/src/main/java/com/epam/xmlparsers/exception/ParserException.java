package com.epam.xmlparsers.exception;

public final class ParserException extends Exception {

	private static final long serialVersionUID = 1L;
	
	public ParserException() {}
	
	public ParserException(String msg) {
		super(msg);
	}
	
	public ParserException(Exception ex) {
		super(ex);
	}
}
