package com.epam.xmlparsers.model;

import java.util.List;

public final class Category {
	
	private String name;
	private List <Subcategory> subcategories;

	public List<Subcategory> getSubcategories() {
		return subcategories;
	}

	public void setSubcategories(List<Subcategory> subcategories) {
		this.subcategories = subcategories;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
