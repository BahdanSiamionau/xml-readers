package com.epam.xmlparsers.model;

import java.util.List;

public final class Subcategory {

	private String name;
	private List <Good> goods;

	public List<Good> getGoods() {
		return goods;
	}

	public void setGoods(List<Good> goods) {
		this.goods = goods;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
