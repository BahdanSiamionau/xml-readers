package com.epam.xmlparsers.command;

import java.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;

import com.epam.xmlparsers.resources.ParserConstants;

public final class IndexCommand implements ICommand {
	
	private static final ResourceBundle properties = ResourceBundle
			.getBundle(ParserConstants.PATH);

	@Override
	public String execute(HttpServletRequest request) {
		return new String(properties.getString(ParserConstants.INDEX_PATH));
	}

}
