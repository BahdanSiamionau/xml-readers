package com.epam.xmlparsers.command;

import java.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;

import com.epam.xmlparsers.parser.ProductsDOMParser;
import com.epam.xmlparsers.resources.ParserConstants;

public final class DOMParserCommand implements ICommand {
	
	private static final ResourceBundle properties = ResourceBundle
			.getBundle(ParserConstants.PATH);

	@Override
	public String execute(HttpServletRequest request) {
		ProductsDOMParser parser = ProductsDOMParser.getInstance();
		request.getSession().setAttribute(ParserConstants.RESPONSE, 
				parser.prepareParser(properties.getString(ParserConstants.XML_PATH)));
		return new String(properties.getString(ParserConstants.INDEX_PATH));
	}
}
