package com.epam.xmlparsers.command;

import java.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;

import com.epam.xmlparsers.parser.ProductsStAXParser;
import com.epam.xmlparsers.resources.ParserConstants;

public final class StAXParserCommand implements ICommand {
	
	private static final ResourceBundle properties = ResourceBundle
			.getBundle(ParserConstants.PATH);

	@Override
	public String execute(HttpServletRequest request) {
		ProductsStAXParser parser = ProductsStAXParser.getInstance();
		request.getSession().setAttribute(ParserConstants.RESPONSE, 
				parser.prepareParser(properties.getString(ParserConstants.XML_PATH)));
		return new String(properties.getString(ParserConstants.INDEX_PATH));
	}
}
