package com.epam.xmlparsers.parser;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.apache.log4j.Logger;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;

import com.epam.xmlparsers.model.Category;
import com.epam.xmlparsers.model.Good;
import com.epam.xmlparsers.model.Subcategory;
import com.epam.xmlparsers.resources.ParserConstants;

public final class ProductsSAXParser extends DefaultHandler implements IProductsParser {
	
	private List<Category> categoryList;
	private	List<Subcategory> subcategoryList;
	private List<Good> goodList;
	private Good good;
	private String currentElement;
	private static final Logger log = Logger.getLogger(ProductsSAXParser.class);
	
	@Override
	public void startDocument() throws SAXException {
		log.info(ParserConstants.PARSING_STARTED + this.getClass().getName());
		categoryList = new ArrayList<Category>();
		currentElement = ParserConstants.PRODUCTS;
	}
	
	@Override
	public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
		switch(localName) {
		case ParserConstants.CATEGORY:
			Category category = new Category();
			category.setName(attributes.getValue(ParserConstants.NAME));
			subcategoryList = new ArrayList<Subcategory>();
			category.setSubcategories(subcategoryList);
			categoryList.add(category);
			break;
		case ParserConstants.SUBCATEGORY:
			Subcategory subcategory = new Subcategory();
			subcategory.setName(attributes.getValue(ParserConstants.NAME));
			goodList = new ArrayList<Good>();
			subcategory.setGoods(goodList);
			subcategoryList.add(subcategory);
			break;
		case ParserConstants.GOOD:
			good = new Good();
			break;
		case ParserConstants.PRODUCER:
			currentElement = ParserConstants.PRODUCER;
			break;
		case ParserConstants.MODEL:
			currentElement = ParserConstants.MODEL;
			break;
		case ParserConstants.DATE_OF_ISSUE:
			currentElement = ParserConstants.DATE_OF_ISSUE;
			break;
		case ParserConstants.COLOR:
			currentElement = ParserConstants.COLOR;
			break;
		case ParserConstants.NOT_IN_STOCK:
			currentElement = ParserConstants.NOT_IN_STOCK;
			good.setPrice(ParserConstants.NOT_IN_STOCK);
			break;
		case ParserConstants.PRICE:
			currentElement = ParserConstants.PRICE;
			break;
		}
	}
	
	@Override
	public void endElement(String uri, String localName, String qName) throws SAXException {
		switch (localName) {
		case ParserConstants.GOOD:
			goodList.add(good);
			break;
		}
	}
	
	@Override
	public void characters(char[] ch, int start, int length) throws SAXException {
		switch (currentElement) {
		case ParserConstants.PRODUCER:
			good.setProducer(new String(ch, start, length));
			break;
		case ParserConstants.MODEL:
			good.setModel(new String(ch, start, length));
			break;
		case ParserConstants.DATE_OF_ISSUE:
			good.setDateOfIssue(new String(ch, start, length));
			break;
		case ParserConstants.COLOR:
			good.setColor(new String(ch, start, length));
			break;
		case ParserConstants.PRICE:
			good.setPrice(new String(ch, start, length));
			break;
		}
	}
	
	@Override
	public void endDocument() throws SAXException {
		log.info(ParserConstants.PARSING_ENDED + this.getClass().getName());
	}

	@Override
	public List<Category> prepareParser(String fileName) {
		try {
			SAXParserFactory factory = SAXParserFactory.newInstance();
			SAXParser parser = factory.newSAXParser();
			XMLReader reader = parser.getXMLReader();
			reader.setFeature(ParserConstants.NAMESPACES, true);
			reader.setContentHandler(this);
			reader.parse(fileName);
		} catch (SAXException | ParserConfigurationException | IOException ex) {
			log.error(ex.getMessage());
		} 
		return this.categoryList;
	}
}
