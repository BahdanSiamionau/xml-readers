package com.epam.xmlparsers.parser;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import org.apache.log4j.Logger;

import com.epam.xmlparsers.model.Category;
import com.epam.xmlparsers.model.Good;
import com.epam.xmlparsers.model.Subcategory;
import com.epam.xmlparsers.resources.ParserConstants;

public final class ProductsStAXParser implements IProductsParser {

	private static final Logger log = Logger.getLogger(ProductsStAXParser.class);
	
	private ProductsStAXParser() {
		
	}
	
	private static final class InstanceHolder {
		private static final ProductsStAXParser instance = new ProductsStAXParser(); 
	}
	
	public static ProductsStAXParser getInstance() {
		return InstanceHolder.instance;
	}
	
	public List<Category> parse(XMLStreamReader reader) {
		List<Category> categoryList = new ArrayList<>();
		List<Subcategory> subcategoryList = new ArrayList<>();
		List<Good> goodList = new ArrayList<>();
		Good good = new Good();
		try {
			String value = "";
			while (reader.hasNext()) {
				switch (reader.next()) {
				case XMLStreamConstants.START_ELEMENT:
					switch (reader.getLocalName()) {
					case ParserConstants.PRODUCTS:
						log.info(ParserConstants.PARSING_STARTED + this.getClass().getName());
						categoryList = new ArrayList<Category>();
						break;
					case ParserConstants.CATEGORY:
						Category category = new Category();
						category.setName(reader.getAttributeValue(null, ParserConstants.NAME));
						subcategoryList = new ArrayList<Subcategory>();
						category.setSubcategories(subcategoryList);
						categoryList.add(category);
						break;
					case ParserConstants.SUBCATEGORY:
						Subcategory subcategory = new Subcategory();
						subcategory.setName(reader.getAttributeValue(null, ParserConstants.NAME));
						goodList = new ArrayList<Good>();
						subcategory.setGoods(goodList);
						subcategoryList.add(subcategory);
						break;
					case ParserConstants.GOOD:
						good = new Good();
						break;
					case ParserConstants.NOT_IN_STOCK:
						good.setPrice(ParserConstants.NOT_IN_STOCK);
						break;
					}
					break;
				case XMLStreamConstants.END_ELEMENT:
					switch (reader.getLocalName()) {
					case ParserConstants.GOOD:
						goodList.add(good);
						break;
					case ParserConstants.PRODUCER:
						good.setProducer(value);
						break;
					case ParserConstants.MODEL:
						good.setModel(value);
						break;
					case ParserConstants.DATE_OF_ISSUE:
						good.setDateOfIssue(value);
						break;
					case ParserConstants.COLOR:
						good.setColor(value);
						break;
					case ParserConstants.PRICE:
						good.setPrice(value);
						break;
					}
					break;
				case XMLStreamConstants.CHARACTERS:
					value = reader.getText();			
					break;
				case XMLStreamConstants.END_DOCUMENT:
					log.info(ParserConstants.PARSING_ENDED + this.getClass().getName());
					break;
				}
			}
		} catch (XMLStreamException ex) {
			System.out.println(ex.getMessage());
		}
		return categoryList;
	}
	
	@Override
	public List<Category> prepareParser(String fileName) {
		List<Category> categoryList = new ArrayList<Category>();
		try {
			InputStream stream = new FileInputStream(fileName);
			XMLInputFactory factory = XMLInputFactory.newInstance();
			XMLStreamReader reader = factory.createXMLStreamReader(stream);
			categoryList = parse(reader);
		} catch (FileNotFoundException | XMLStreamException ex) {
			log.error(ex.getMessage());
		}
		return categoryList;
	}
}
