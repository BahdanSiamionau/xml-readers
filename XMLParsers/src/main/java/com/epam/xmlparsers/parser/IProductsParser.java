package com.epam.xmlparsers.parser;

import java.util.List;

import com.epam.xmlparsers.model.Category;

public interface IProductsParser {
	public List <Category> prepareParser(String fileName);
}
