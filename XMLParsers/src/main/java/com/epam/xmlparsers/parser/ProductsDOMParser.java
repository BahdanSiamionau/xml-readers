package com.epam.xmlparsers.parser;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.epam.xmlparsers.exception.ParserException;
import com.epam.xmlparsers.model.Category;
import com.epam.xmlparsers.model.Good;
import com.epam.xmlparsers.model.Subcategory;
import com.epam.xmlparsers.resources.ParserConstants;

public final class ProductsDOMParser implements IProductsParser {
	
	private static final Logger log = Logger.getLogger(ProductsDOMParser.class);
	
	private ProductsDOMParser() {
		
	}
	
	private static final class InstanceHolder {
		private static final ProductsDOMParser instance = new ProductsDOMParser(); 
	}
	
	public static ProductsDOMParser getInstance() {
		return InstanceHolder.instance;
	}

	public List <Category> parse(Element root) throws ParserException {
		log.info(ParserConstants.PARSING_STARTED + this.getClass().getName());
		List <Category> categoryList = new ArrayList<Category>();
		NodeList categories = root.getElementsByTagName(ParserConstants.CATEGORY);
		int categoriesLength = categories.getLength();
		for (int i = 0; i < categoriesLength; i++) {
			Element categoryEl = (Element) categories.item(i);
			Category category = new Category();
			category.setName(categoryEl.getAttribute(ParserConstants.NAME));
			List <Subcategory> subcategoryList = new ArrayList <Subcategory> ();
			category.setSubcategories(subcategoryList);
			categoryList.add(category);
			NodeList subcategories = categoryEl.getElementsByTagName(ParserConstants.SUBCATEGORY);
			int subcategoriesLength = subcategories.getLength();
			for (int j = 0; j < subcategoriesLength; j++) {
				Element subcategoryEl = (Element) subcategories.item(j);
				Subcategory subcategory = new Subcategory();
				subcategory.setName(subcategoryEl.getAttribute(ParserConstants.NAME));
				List <Good> goodList = new ArrayList<Good>();
				subcategory.setGoods(goodList);
				subcategoryList.add(subcategory);
				NodeList goods = subcategoryEl.getElementsByTagName(ParserConstants.GOOD);
				int goodsLength = goods.getLength();
				for (int k = 0; k < goodsLength; k++) {
					Element goodEl = (Element) goods.item(k);
					Good good = new Good();
					good.setProducer(getChildValue(goodEl, ParserConstants.PRODUCER));
					good.setModel(getChildValue(goodEl, ParserConstants.MODEL));
					good.setDateOfIssue(getChildValue(goodEl, ParserConstants.DATE_OF_ISSUE));
					good.setColor(getChildValue(goodEl, ParserConstants.COLOR));
					good.setPrice(getChildValue(goodEl, ParserConstants.PRICE) != null 
							? getChildValue(goodEl, ParserConstants.PRICE) 
									: ParserConstants.NOT_IN_STOCK);
					goodList.add(good);
				}
			}
		}
		log.info(ParserConstants.PARSING_ENDED + this.getClass().getName());
		return categoryList;
	}
	
	private String getChildValue(Element parent,	String childName) {
		NodeList nodeList =	parent.getElementsByTagName(childName);
		Element child = (Element) nodeList.item(0);
		if (child != null) {
			String value = child.getTextContent();
			return value;
		}
		return null;
	}

	
	public List <Category> prepareParser(String fileName) {
		List <Category> categoryList = new ArrayList<Category>();
		try {
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			Document document = builder.parse(fileName);
			categoryList = parse(document.getDocumentElement());
		} catch (ParserConfigurationException | IOException | 
				SAXException | ParserException ex) {
			log.error(ex.getMessage());
		}
		return categoryList;
	}
}
