package com.epam.xmlparsers.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.epam.xmlparsers.command.ICommand;
import com.epam.xmlparsers.helper.RequestHelper;

@WebServlet("/ParsersController")
public final class ParserController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	public ParserController() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		performTask(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		performTask(request, response);
	}
	
	private void performTask(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		RequestHelper requestHelper = RequestHelper.getInstance();
		ICommand command = requestHelper.getCommand(request);
		String page = command.execute(request); 
		RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(page);
		dispatcher.forward(request, response);
	}
}
