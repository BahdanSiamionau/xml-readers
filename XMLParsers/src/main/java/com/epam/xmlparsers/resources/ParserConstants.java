package com.epam.xmlparsers.resources;

public final class ParserConstants {

	public static final String PATH = "path";
	public static final String XML_PATH = "xml.file.path";
	public static final String RESPONSE = "response";
	public static final String INDEX_PATH = "jsp.index.path";
	public static final String SAX = "sax";
	public static final String STAX = "stax";
	public static final String DOM = "dom";
	public static final String INDEX = "index";
	public static final String COMMAND = "command";
	public static final String PARSING_STARTED = "Parsing started with ";
	public static final String PARSING_ENDED = "Parsing ended with ";
	public static final String PRODUCTS = "Products";
	public static final String CATEGORY = "Category";
	public static final String SUBCATEGORY = "Subcategory";
	public static final String GOOD = "Good";
	public static final String PRODUCER = "Producer";
	public static final String MODEL = "Model";
	public static final String DATE_OF_ISSUE = "Date-of-issue";
	public static final String COLOR = "Color";
	public static final String PRICE = "Price";
	public static final String NOT_IN_STOCK = "Not-in-stock";
	public static final String NAME = "name";
	public static final String NAMESPACES = "http://xml.org/sax/features/namespaces";
	
	private ParserConstants() {
		
	}
}