<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Parsers</title>
</head>
<body>
	<form name="dom_form" action="index" method="post">
		<input type="hidden" name="command" value="dom"/> 
		<a href="javascript:document.forms['dom_form'].submit()">DOM parser</a>
	</form>
	<form name="sax_form" action="index" method="post">
		<input type="hidden" name="command" value="sax"/> 
		<a href="javascript:document.forms['sax_form'].submit()">SAX parser</a>
	</form>
	<form name="stax_form" action="index" method="post">
		<input type="hidden" name="command" value="stax"/> 
		<a href="javascript:document.forms['stax_form'].submit()">StAX parser</a>
	</form>
	<c:forEach var="category" items="${response}">
		<c:out value="${category.name}"/>
		<br>
		<c:forEach var="subcategory" items="${category.subcategories}">
			&nbsp;&nbsp;&nbsp;&nbsp;
			<c:out value="${subcategory.name}"/>
			<br>
			<c:forEach var="good" items="${subcategory.goods}">
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<c:out value="${good.producer}"/>
				<c:out value="${good.model}"/>
				<c:out value="${good.dateOfIssue}"/>
				<c:out value="${good.color}"/>
				<c:out value="${good.price}"/>
				<br>
			</c:forEach>
		</c:forEach>
	</c:forEach>
</body>
</html>